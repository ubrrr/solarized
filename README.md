# Dependencies
- noto-fonts
- ttf-font-awesome
- picom-tryone-git "from the aur if using arch"

# Installation
As root run:
sh install.sh

# Put these in your .xinitrc
- picom --experimental-backends &
- slstatus &
- exec dwm
