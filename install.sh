#!/bin/sh

if [ "$(id -u)" = 1000 ]; then
	echo Must be root to run the script
	exit 1
fi
error() { \
	clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

cd dwm
make clean install
cd ..
cd st
make clean install
cd ..
cd slstatus
make clean install
cd ..
cd dmenu
make clean install
cd ..
