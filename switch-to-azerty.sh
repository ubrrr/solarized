#!/bin/sh

if [ "$(id -u)" = 0 ]; then
        echo You should not run this script as root!
        exit 1
fi
error() { \
        clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

[ "$(pwd)" != ~/solarized ] && echo "Must be in the solarized directory to run the script!" && exit 1
if [ -f switch-to-azerty.sh ]; then
#Adds Azerty
sed -i '146 s/...//; 197 s/...$//' dwm/config.def.h
#Removes Qwerty
sed -i '92 s/^/\/* /; 143 s/$/ *\//' dwm/config.def.h
mv switch-to-azerty.sh switch-to-qwerty.sh

else

#Adds Qwerty
sed -i '92 s/...//; 143 s/...$//' dwm/config.def.h
#Removes Azerty
sed -i '146 s/^/\/* /; 197 s/$/ *\//' dwm/config.def.h
mv switch-to-qwerty.sh switch-to-azerty.sh

fi

cd dwm
cp config.def.h config.h
sudo make clean install
cd ..
